let winner = null;

function buildCardCar(id, name, imgUrl) {
    const carTemplate = document.querySelector('#carCard');

    let node = carTemplate.cloneNode(true);
    let idNode = node.content.querySelector('.card__id');
    idNode.innerHTML = id;
    let imgNode = node.content.querySelector('.card__img');
    imgNode.src = imgUrl;
    let nameNode = node.content.querySelector('.card__name');
    nameNode.innerHTML = name;

    document.querySelector('#carCardList').appendChild(node.content);
}

function move(element, name, direction, velocity) {
    let distance = 1000
    var topOrLeft = (direction === "left" || direction === "right") ? "left" : "top";
    var isNegated = (direction === "up" || direction === "left");
    if (isNegated) {
        distance *= -1;
    }
    var elStyle = window.getComputedStyle(element);
    var value = elStyle.getPropertyValue(topOrLeft).replace("px", "");
    let destination = Number(value) + distance;
    var frameDistance = velocity * 30 / distance;

    function moveAFrame() {
        elStyle = window.getComputedStyle(element);
        value = elStyle.getPropertyValue(topOrLeft).replace("px", "");
        var newLocation = Number(value) + frameDistance;
        var beyondDestination = ((!isNegated && newLocation >= destination)
            || (isNegated && newLocation <= destination))
            || winner !== null;
        if (beyondDestination) {
            if (winner === null) {
                element.style[topOrLeft] = destination + "px";
                winner = name;
                document.dispatchEvent(new CustomEvent('winner', {
                    detail: {
                        winner: winner
                    }
                }))
            }
            clearInterval(movingFrames);
        } else {
            element.style[topOrLeft] = newLocation + "px";
        }
    }

    var movingFrames = setInterval(moveAFrame, 10);
}