Prueba técnica SENIOR FRONTEND coches.com
=========================================

Esta prueba consiste en que representes el diseño que te facilitamos y apliques ciertas funcionalidades partiendo de código sin ningún tipo de buenas prácticas o legacy. 

Los diseños los encontrarás en la carpeta "design". Hemos incluído también el diseño original en Sketch, pero si no dispones de él (lo más normal), también te hemos guardado las imágenes en la carpeta "png". A continuación te detallamos que encontrarás:

- "page.png" es la página que debes construir. 
- "modal.png" muestra el modal que se abrirá al pulsar sobre alguna de las pastillas de los coches.

En la carpeta "src" hemos dejado un HTML con una estructura básica, así como las imágenes que puedas necesitar en "imgs", y dos ficheros JS que pueden serte útiles para arrancar con una funcionalidad básica.

Indicaciones
------------
Se trata de una carrera de los míticos coches del cine y la televisión. 

En la página podemos ver los coches que tenemos disponibles para la carrera, los cuales puedes obtener a través del siguiente endpoint: https://6076c6591ed0ae0017d69c71.mockapi.io/coolCar aquí tendrás disponible el id y si está activo o no, ya que solo debemos mostrar aquellos que estén activos. Para obtener el resto de información de cada coche, debes añadir el id al final https://6076c6591ed0ae0017d69c71.mockapi.io/coolCar/[id], por ejemplo: https://6076c6591ed0ae0017d69c71.mockapi.io/coolCar/5

Al clickar sobre cualquiera de las pastillas de los coches ubicados en la sección con el listado, se abrirá un modal con la info de ese coche, así como la posibilidad de añadirlo o quitarlo de la carrera a través de dos botones. Si se añade, se agregará a la sección PARRILLA DE SALIDA, donde en un primer momento aparecerá únicamente la imagen de START/FINISH (sin ningún coche). Si se saca, pues desaparecerá de esa sección.

Una vez seleccionados los coches que van a participar en la carrera, el usuario puede pulsar sobre el botón ¡Empezar carrera!, y la carrera empezará (dispones de la función ´move()´ en el fichero "utils.js" para no complicarte con esto), dando a uno de ellos como ganador, y mostrando un alert.

Si vas sobrado de tiempo y quieres tomarte la libertad de hacer un POST al endpoint: https://6076c6591ed0ae0017d69c71.mockapi.io/race con el ganador, el perdedor y la fecha de la carrera, ¡fetén! pero no es el objetivo :)

------------

Tómate la libertad de reorganizar todo (o como creas que debería ser): carpetas, ficheros, renombrado, funciones, naming, etc. Retoca, reestructura, refactoriza, ordena totalmente a tu gusto, se trata de que te sientas cómodo para trabajar. Siéntete libre de modificar todo, según más te convenga para llegar al fin. 

Nos gustaría mucho que fuese responsive y que lo adaptaras para todos los dispositivos, bajo tu criterio. No te aportamos diseño para tablet y mobile, únicamente de desktop, así que hazlo como mejor creas que debería ser. No es necesario que apliques las mismas fuentes, tamaños, etc. buscamos un aproximado al diseño que te damos.

Si que nos gustaría que la funcionalidad fuese toda en Vanilla JavaScript y no con el uso de ningún framework.

Estamos a tu disposición para cualquier duda que pueda surgirte ;)

¡Esperamos lo mejor de ti!
¡ÁNIMO y MUCHA SUERTE!
coches.com